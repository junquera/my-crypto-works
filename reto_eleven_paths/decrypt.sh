base64 -d fek1.txt > fek1.raw
base64 -d fek2.txt > fek2.raw
openssl rsautl -inkey rsa_key1.pem -decrypt -in fek1.raw > key1.raw
openssl rsautl -inkey rsa_key2.pem -decrypt -in fek2.raw > key2.raw
openssl aes-256-cbc -d -a -in ef1.txt -kfile key1.raw -out message1.txt
openssl aes-256-cbc -d -a -in ef2.txt -kfile key2.raw -out message2.txt
