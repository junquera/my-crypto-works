from OpenSSL import crypto
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES

import gmpy2
import base64
import hashlib
import codecs

# Open certificate
with open('cert1.txt') as f:
    cert1 = crypto.load_certificate(crypto.FILETYPE_PEM, f.read())

pub1 = cert1.get_pubkey()
pk1 = pub1.to_cryptography_key()

n1 = pk1.public_numbers().n

with open('cert2.txt') as f:
    cert2 = crypto.load_certificate(crypto.FILETYPE_PEM, f.read())

pub2 = cert2.get_pubkey()
pk2 = pub2.to_cryptography_key()

n2 = pk2.public_numbers().n

p = gmpy2.gcd(n1, n2)

q1 = n1 // p
q2 = n2 // p

e1 = pk1.public_numbers().e
e2 = pk2.public_numbers().e

fi1 = (p - 1) * (q1 - 1)
fi2 = (p - 1) * (q2 - 1)

d1 = gmpy2.invert(e1, fi1)
d2 = gmpy2.invert(e2, fi2)

# Generate RSA key with parameters
key1 = RSA.construct((n1, e1, int(d1), int(p), int(q1)))
key2 = RSA.construct((n2, e2, int(d2), int(p), int(q2)))

with open('rsa_key1.pem', 'wb+') as f:
    f.write(key1.exportKey('PEM'))

with open('rsa_key2.pem', 'wb+') as f:
    f.write(key2.exportKey('PEM'))
