# Concurso #EquinoxRoom111

Adjunto los archivos `ransom_file_00{3,4}.txt`, `code.py` (con todo lo que he hecho en python), `decrypt.sh` y los archivos `message{1,2}.txt` con los resultados. También `README.md` (el código markdown con el que he generado este writeup).

## Introducción

Se pide descifrar dos archivos cifrados por un ransomware, conociendo el formato de archivo generado y los cifrados utilizados:

1. Cifra el archivo con una clave simétrica usando `aes-256-cbc`
2. Cifra la clave simétrica con RSA (se puede descifrar con la privada del certificado indicado en el archivo)

Indican que los dos certificados se han generado con un generador de números poco aleatorios.

## Desarrollo

Primero extraigo todos los archivos para poder trabajar con ellos:

- Certificate es *certX.txt*

- FileEncryptionKey es *fekX.txt*

- EncryptedFile es *efX.txt*

### Paso 1 - Estudio de los certificados

Para analizar los certificados los cargo usando la librería de `OpenSSL` para python:

```python
from OpenSSL import crypto

# Open certificate
with open('cert1.txt') as f:
    cert1 = crypto.load_certificate(crypto.FILETYPE_PEM, f.read())

pub1 = cert1.get_pubkey()
pk1 = pub1.to_cryptography_key()

n1 = pk1.public_numbers().n

with open('cert2.txt') as f:
    cert2 = crypto.load_certificate(crypto.FILETYPE_PEM, f.read())

pub2 = cert2.get_pubkey()
pk2 = pub2.to_cryptography_key()

n2 = pk2.public_numbers().n
```

La única forma (más eficiente que la fuerza bruta) de romper el cifrado es que haya una vulnerabilidad en la generación de los primos de RSA. Cada *n* se ha generado con un par de primos *p* y *q* tal que:

```
n = p * q
```

La fuerza de RSA reside en que la clave privada *d* es la inversa modular de la clave pública *e* en base a la función de Euler (*fi*) de *n*, y la factorización de *n* es muy complicada computacionalmente. Tendríamos que:

```
fi = (p - 1) * (q - 1)
```

Por lo que para calcular la clave privada necesitamos encontrar alguna forma eficiente de descomponer *n*.

Probando a extraer factor común (ver [Understanding Common Factor Attacks:
An RSA-Cracking Puzzle](http://www.loyalty.org/~schoen/rsa/)) veo que sí que lo tienen:

```python
import gmpy2
p = gmpy2.gcd(n1, n2)
```

Así que podemos obtener las q's dividiendo:

```python
>>> q1 = n1//p
mpz(168999454582076769166171296262031363190509712854556621140566989268530359732648373245477904973373795944647809456911297816213634383836098311582384316977235911034191513620629957914294638593003328522024368048563397894983889735243069119328805856150524210895895589053247051038510896966980988109430420359657520256559)
>>> q2 = n2//p
mpz(158912395212203769897670866604560368725245513877133316683880686062044836433419221351644075135097894553867898154790559579813724419743683183352124704700711700019578057479578581346099942744009803230594284977886986043440112192788852820519758674994367312397545424170912474185898190237050620297342018220865274063139)
```

Para verificar que todo ha funcionado:

```python
>>> gmpy2.is_prime(q1) and ((p*q1) == n1) and gmpy2.is_prime(q2) and ((p*q2) == n2)
True
```

Ha funcionado =)

## Paso 2 - Descifrar la clave simétrica

Tras darle muchas vueltas, no consigo descifrarla por código. Es decir, la descifro pero no me funciona para romper el cifrado del archivo.

```python
>> with open('fek1.txt') as f:
>>    fek1 = base64.b64decode(f.read())

>> print(key1.decrypt(fek1))
b'\x02R\xa6\x14\x93<\x85\xdd~\xe1\xcdRD\xab\xc2W\x02\xc3\xaa\xb1\xd8\xe9A\x80}\xb6 \xe8\xab\xbcx\xddqSU\xa2\xad\xd1?h\x97\xfb\x15\xbb>\\\x91\xd3&|\xf8\xbb\xd1\xc9\xfe\x809\xb3qR\x13\x03QV\xaf\'\x1a\'\x9f\xa1\xef{sZ\xf1\xcb|t4-\xf0\x80\xb9\\\xe1l\xa8R b]sy\x8d\xbf\x0c\xa8_h\xb2\xb1L\xf0X\xa8#\xf1"v[.{\x14\x12Y\xe3\x87%L\x1b"|V\x81!\x06\x84xb\xd5\xe0\xb5\x85\xb90\xd1/`\xd5\xd7\x1c@\xa9\x97E\xe3\xb3AP-\xfd\x88\x7f\xba\xce&\x95\xb2\xbd\x8b\xab\xb71Y\x1cPf\x03X?\xa5\x98-\xa2v\x88\t\xf3\x93\x89jM\'\ni\xfd%\x8d\xd9\xd6D\xa6H\xc01\xe1\xc7\\y[qu\xb5\xd9\xf2\x9c:"E\xb5\xb2\x86(\x9f6\x94\x1b\xb4\xf8\xf0\xa8\x92\x00\x17\xb8\xbc\x00E\t_\xc6!\x8aR\x18=<\xa7\xa3M\xf7\\\xadO\xc9\x82\xc8\xb4\xd1R \xa0OW['
```

Como esta clave es demasiado larga para AES (256 bits son 32 caracteres) imagino que se habrá utilizado para cifrar mediante alguna derivación (*pbkdf2*, algún hash...), pero no me funciona nada. Pruebo con todos estos mecanismos a descifrar el AES, pero siempre me salen datos binarios, en ningún momento un texto que pueda ser el resultado.

Finalmente, al quitar el encoding del archivo cifrado veo que tiene un metadato:

```bash
junquera@delfin:~/reto_eleven_paths$ base64 -d ef1.txt
Salted__~�JFJ�8ve\�Y��� ��X&	EnM�6Õ�:Z��UR��
$����Q7�\
```

Ese `Salted__` nos indica que puede haber sido generado con *OpenSSL* (que implementa determinadas rutinas de forma particular).

## Paso 3 - Descifrar todo

Teniendo las claves RSA, puedo descifrarlo todo por consola. Escribo el siguiente archivo:

```bash
# decrypt.#!/bin/sh
base64 -d fek1.txt > fek1.raw
base64 -d fek2.txt > fek2.raw
openssl rsautl -inkey rsa_key1.pem -decrypt -in fek1.raw > key1.raw
openssl rsautl -inkey rsa_key2.pem -decrypt -in fek2.raw > key2.raw
openssl aes-256-cbc -d -a -in ef1.txt -kfile key1.raw -out message1.txt
openssl aes-256-cbc -d -a -in ef2.txt -kfile key2.raw -out message2.txt
```

Y obtengo:

```bash
junquera@delfin:~/reto_eleven_paths$ bash decrypt.sh
*** WARNING : deprecated key derivation used.
Using -iter or -pbkdf2 would be better.
*** WARNING : deprecated key derivation used.
Using -iter or -pbkdf2 would be better.

junquera@delfin:~/reto_eleven_paths$ cat message1.txt
Capacicard rocks! (capacicard.e-paths.com)

junquera@delfin:~/reto_eleven_paths$ cat message2.txt
DIARIO rocks! (diario.e-paths.com)
```

Los mensajes que el ransomware ha cifrado son:

- **Capacicard rocks! (capacicard.e-paths.com)**

- **DIARIO rocks! (diario.e-paths.com)**
